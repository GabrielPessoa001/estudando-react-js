# README #

Esse é um repositório de estudos criado por mim para alocar parte do meu desenvolvimento em ReactJS e React Native.

#Projetos iniciados e suas propostas:

----Componentes - Base 2 -> Fundamenta-se na base 1, mas com funcionalidades e telas diferentes que não seguem os padrões especificados na base de estudos anterior. Tenho como foco aprender a utilizar APIs de terceiros para consulta de dados.
---- Libs usadas -> Booststrap, React-Icons, React-Router e EmailJS

#Projetos concluídos e suas propostas:

---- Componentes - Base 1 -> Meu primeiro projeto utilizando apenas React e Bootstrap 4 como toolkit para formação de um software.
-------- Nele, faço a criação de alguns componentes, utilizo a estilização padrão CSS juntamente com Bootstrap, encaixo o EmailJS como lib primária para envio de e-mail com pedidos, adiciono popovers e um modal e crio rotas específicas para cada página (atualmente, duas).