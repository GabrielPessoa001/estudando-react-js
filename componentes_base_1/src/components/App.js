import React from 'react';

import Menu from './Menu';
import Dashboard from './Dashboard';
import Candy from './Candy';

import { Switch, Route } from 'react-router-dom'

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Menu />
        <Switch>
          <Route exact path='/' component={Dashboard}/>
          <Route path='/doces' component={Candy}/>
        </Switch>
      </div>
    )
  }
}