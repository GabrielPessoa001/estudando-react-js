import React, { useState, useRef } from 'react';

import './../stylesheets/Candy.css';

import barca from './../images/barca.jpg';
import produtos from './../images/produtos.jpg';

import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from 'react-bootstrap/Carousel';

import Footer from './Footer';

export default class Candy extends React.Component {
  render() {
    return (
      <div>
        <SingularCandy />
        <Footer />
      </div>
    )
  }
}

class SingularCandy extends React.Component {
  render() {
    return (
      <div className="div_carousel">
        <Carousel>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src={produtos}
              alt="Third slide"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src={barca}
              alt="Third slide"
            />
          </Carousel.Item>

        </Carousel>
      </div>
    )
  }
}
