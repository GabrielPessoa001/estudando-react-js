import React from 'react';

import './../stylesheets/Dashboard.css'

import { FaInstagram } from 'react-icons/fa';
import { FaWhatsapp } from 'react-icons/fa';
import { FaMapMarkerAlt } from 'react-icons/fa';

export default class Dashboard extends React.Component {
  render() {
    return (
      <div className="divisor_dashboard">
        <div className="border_dashboard">
          <h1 className="FaInstagram"><FaInstagram /></h1>
          <p className="instagram_dashboard">@_LaBelly</p>
          <br/>
          <h1 className="FaWhatsapp"><FaWhatsapp /></h1>
          <p className="contact_dashboard">Número que não sei ainda</p>
          <br/>
          <h1 className="FaMapMarkerAlt"><FaMapMarkerAlt /></h1>
          <p className="address_dashboard">Address</p>
          <br/><br/>
          <p className="address_dashboard">A La Belly Doces é especializada na produção de doces. Confira nossos doces ao lado!</p>
        </div>
      </div>
    )
  }
}