import React from 'react';

import './../stylesheets/Email.css'

import { FaPaperPlane } from "react-icons/fa";

export default class Email extends React.Component {
  constructor(props) {
    super(props);

    this.state = { candy: '', to_email: '', name: '', address: '', phone: '' };

    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeCandy = this.handleChangeCandy.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePhone = this.handleChangePhone.bind(this);
    this.handleChangeAddress = this.handleChangeAddress.bind(this);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <form className="test-mailing">
        <h1 className="title_email">Solicitar e-mail com pedido</h1>
        <fieldset>
          <input
            onChange={this.handleChangeName}
            placeholder="Adicione seu nome"
            required
            value={this.state.name}
            style={{width: '50%', height: '35px'}}
          />
          <input
            onChange={this.handleChangePhone}
            placeholder="Adicione seu telefone"
            required
            value={this.state.phone}
            style={{width: '50%', height: '35px'}}
          />
          <input
            onChange={this.handleChangeEmail}
            placeholder="Adicione seu e-mail"
            required
            value={this.state.to_email}
            style={{width: '100%', height: '35px'}}
          />
          <input
            onChange={this.handleChangeAddress}
            placeholder="Adicione seu endereço"
            required
            value={this.state.address}
            style={{width: '100%', height: '35px'}}
          />
          <textarea
            onChange={this.handleChangeCandy}
            placeholder="Adicione seus pedidos, seu endereço e seu número para entrarmos em contato"
            required
            value={this.state.candy}
            style={{width: '100%', height: '100px'}}
          />
        </fieldset>
        <FaPaperPlane className="FaPaperPlane" onClick={this.handleSubmit}/>
      </form>
      )
  }

  handleChangeEmail(event) {
    this.setState({to_email: event.target.value})
  }

  handleChangeCandy(event) {
    this.setState({candy: event.target.value})
  }

  handleChangeName(event) {
    this.setState({name: event.target.value})
  }

  handleChangeAddress(event) {
    this.setState({address: event.target.value})
  }

  handleChangePhone(event) {
    this.setState({phone: event.target.value})
  }

  handleSubmit (event) {
    const templateId = 'template_VzwiFi1Y';

    if (this.state.candy && this.state.to_email && this.state.name && this.state.address && this.state.phone) {
      this.sendFeedback(templateId, {message_html: this.state.candy, to_email: this.state.to_email, from_name: this.state.name, address: this.state.address, phone: this.state.phone})
    } else {
      alert('Campos obrigatórios em falta')
    }

  }

  sendFeedback (templateId, variables) {
    window.emailjs.send(
      'gmail', templateId, variables
    ).then(res => {
      alert('Email successfully sent!')
    })
    // Handle errors here however you like, or use a React error boundary
    .catch(err => alert('Digite um e-mail válido, por favor'))
  }
}