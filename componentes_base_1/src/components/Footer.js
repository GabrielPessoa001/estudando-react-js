import React, { useState, useRef } from 'react';

import './../stylesheets/Footer.css';
import './../stylesheets/Popover.css';
import './../stylesheets/Modal.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import { FaCalendarAlt } from 'react-icons/fa';
import { FaShippingFast } from 'react-icons/fa';
import { FaHourglassHalf } from 'react-icons/fa';

import Popover from 'react-bootstrap/Popover';
import Button from 'react-bootstrap/Button';
import Overlay from 'react-bootstrap/Overlay';
import Modal from 'react-bootstrap/Modal';

import Email from './Email';

export default class Cardapio extends React.Component {
  render() {
    return (
      <div className="footer_candy">
        <Funcionamento />
        <Precos />
        <Entregas />
      </div>
    )
  }
}

function Precos() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div className="precos_footer" >
        <h1 className="FaCalendarAlt" onClick={handleShow}><strong><FaCalendarAlt /></strong></h1>
        <p className="cardapio_cardapio"><strong>Cardápio</strong></p>
      </div>

      <Modal className="modal_modal" show={show} onHide={handleClose}>
        <Modal.Header closeButton className="header_modal">
          <Modal.Title className="title_modal">Conferir cardápio</Modal.Title>
        </Modal.Header>
        <Modal.Body className="body_modal">
          <p className="candy_modal">Mousse de maracujá - R$ 3,00</p>
          <p className="candy_modal">Mousse de limão - R$ 3,00</p>
          <p className="candy_modal">Mousse de morango - R$ 3,00</p>
          <p className="candy_modal">Mousse de leite ninho - R$ 3,00</p>
          <p className="candy_modal">Bombom no pote - R$ 5,00</p>
          <p className="candy_modal">Barca especial - R$ 20,00</p>
          <Email />
        </Modal.Body>
      </Modal>
    </>
  );
}

function Funcionamento() {
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);
  const ref = useRef(null);

  const handleClick = (event) => {
    setShow(!show);
    setTarget(event.target);
  };

  return (
    <div ref={ref}>

      <div className="funcionamento_footer" >
        <h1 className="FaHourglassHalf" onClick={handleClick}><strong><FaHourglassHalf /></strong></h1>
        <p className="funcionamento_cardapio"><strong>Funcionamento</strong></p>
      </div>

      <Overlay
        show={show}
        target={target}
        placement="top"
        container={ref.current}
        containerPadding={40}
      >
        <Popover id="popover-contained">
          <Popover.Content className="funcionamento_popover_content">
            <h1 className="h1_popover">Terça a Domingo</h1>
            <p className="p_popover">Das 11h às 18h</p>
          </Popover.Content>
        </Popover>
      </Overlay>
    </div>
  );
}

function Entregas() {
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);
  const ref = useRef(null);

  const handleClick = (event) => {
    setShow(!show);
    setTarget(event.target);
  };

  return (
    <div ref={ref}>

      <div className="entregas_footer"  >
        <h1 className="FaShippingFast" onClick={handleClick} ><strong><FaShippingFast /></strong></h1>
        <p className="entregas_cardapio"><strong>Entregas</strong></p>
      </div>

      <Overlay
        show={show}
        target={target}
        placement="top"
        container={ref.current}
        containerPadding={20}
      >
        <Popover id="popover-contained">
          <Popover.Content className="entregas_popover_content">
            <h1 className="h1_popover">Timbó</h1><p className="p_popover">R$ 1,00</p>
            <h1 className="h1_popover">Jereissati I</h1><p className="p_popover">R$ 1,00</p>
            <h1 className="h1_popover">Outras localidades</h1><p className="p_popover">preço variável</p>
          </Popover.Content>
        </Popover>
      </Overlay>
    </div>
  );
}