import React from 'react';

import logo from './../images/logomarca.jpg';

import './../stylesheets/Menu.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export default class Menu extends React.Component {
  render() {
    return (
      <div>
        <Content />
      </div>
    )
  }
}

class Content extends React.Component {
  render() {
    return (
      <div>
        <div className="navbar_top">
          <strong><a className="informations_menu" href="/">Loja</a></strong>
          <img className="image_logomarca" src={logo}></img>
          <strong><a className="candys_menu" href="/doces">Doces</a></strong>
        </div>
      </div>
    )
  }
}