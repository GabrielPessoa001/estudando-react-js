import React from 'react';

import Header from './Header';
import Footer from './Footer';
import Dashboard from './Dashboard';
import Cardapio from './Cardapio';

import { Switch, Route } from 'react-router-dom';

export default class App extends React.Component {
  render() {
    return (
      <div>
       <Header />
        <Switch>
            <Route exact path='/' component={Dashboard}/>
            <Route exact path='/cardapio' component={Cardapio}/>
        </Switch>
        <Footer />
      </div>
    )
  }
}