import React from 'react';

import Calendar from 'react-calendar';

import 'bootstrap/dist/css/bootstrap.min.css';

import './../stylesheets/Calendario.css';
import './../stylesheets/responsividade/Responsividade_Calendario.css';

export default class Calendario extends React.Component {
  state = {
    date: new Date(),
  }
 
  onChange = date => this.setState({ date })
 
  render() {
    return (
      <div>
        <Calendar
          onChange={this.onChange}
          value={this.state.date}
        />
      </div>
    );
  }
}