import React from 'react';

import './../stylesheets/Dashboard.css';
import './../stylesheets/responsividade/Responsividade_Dashboard.css';

import logo_marca from './../images/logomarca.jpg';
import barca from './../images/barca.jpg';
import produtos from './../images/produtos.jpg';

import Carousel from 'react-bootstrap/Carousel';

export default class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <Content />
      </div>
    )
  }
}

class Content extends React.Component {
  render() {
    return (
      <div className="divisor_dashboard">

        <div className="divisor_title">
          <h2 className="title_dashboard">Informações</h2>
          <h2 className="title_dashboard">Nossos produtos</h2>
        </div>

        <div className="divisor_content">

          <div className="divisor_informations">
            <p className="item_informations">Instagram: </p>
            <p className="item_informations">Telefone: </p>
            <p className="item_informations">Endereço: </p>
          </div>

          <Carousel className="carousel_dashboard">
            <Carousel.Item>
              <img
                className="d-block w-100 imagens_dashboard"
                src={produtos}
                alt="Mousses caseiros e gostosos"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100 imagens_dashboard"
                src={barca}
                alt="Barca de dia dos namorados grande, barata e muito gostosa"
              />
            </Carousel.Item>
          </Carousel>

        </div>

      </div>
    )
  }
}