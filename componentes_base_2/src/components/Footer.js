import React from "react";
 
import './../stylesheets/Footer.css';
import './../stylesheets/responsividade/Responsividade_Footer.css';

export default class Footer extends React.Component {
  render() {
    return (
      <div>
        <Bottom />
      </div>
    );
  }
}

class Bottom extends React.Component {
  render() {
    return (
      <div className="divisor_bottom">
        <h1 className="title_bottom">Trazendo a doçura para o seu dia a dia</h1>
      </div>
    )
  }
}