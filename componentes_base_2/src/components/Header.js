import React from "react";

import './../stylesheets/Header.css';
import './../stylesheets/responsividade/Responsividade_Header.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import Dropdown from 'react-bootstrap/Dropdown';

import { FaCog } from "react-icons/fa";

export default class Header extends React.Component {
  render() {
    return (
      <div>
        <Menu />
      </div>
    );
  }
}

class Menu extends React.Component {
  render() {
    return (
      <div  className="divisor_header">
        <a href="/" className="title_menu">La Belly Doces</a>
        <a className="title_item"></a>

        <a href="cardapio" className="title_item">Cardápio</a>
        <a className="title_item"></a>
        <a className="title_item"></a>
                <a className="title_item"></a>

        <Dropdown className="title_item">
          <Dropdown.Toggle>
            <FaCog className="Dropdown"/>
          </Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item className="dropdown_icon">Tema escuro: desativado</Dropdown.Item>
            <Dropdown.Item className="dropdown_icon">Linguagem: português</Dropdown.Item>
            <Dropdown.Item className="dropdown_icon"></Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    )    
  }
}