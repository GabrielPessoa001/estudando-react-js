import React, { Component } from 'react';
import Calculadora from './Calculadora';

class App extends Component {
  render() {
    return (
      <div>
        <Calculadora />
      </div>
    )
  }
}

export default App;