import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './Calculadora.css'

import Somar from './Somar';
import Multiplicar from './Multiplicar';
import Diminuir from './Diminuir';
import Dividir from './Dividir';

class Calculadora extends Component {
  render() {
    return (
      <div>

        <h1 className="title_calculadora">Calculadora</h1>

        <div className="div_links">

        <a href="/somar" className="link_operacao">+</a>
        <a href="/multiplicar" className="link_operacao">x</a>
        <a href="/diminuir" className="link_operacao">-</a>
        <a href="/dividir" className="link_operacao">÷</a>

        </div>

        <Router>
          <Switch>
            <Route path="/somar">
              <div className="content_operacao">
                <Somar />
              </div>
            </Route>
            <Route path="/multiplicar">
              <div className="content_operacao">
                <Multiplicar />
              </div>
            </Route>
            <Route path="/diminuir">
              <div className="content_operacao">
                <Diminuir />
              </div>
            </Route>
            <Route path="/dividir">
              <div className="content_operacao">
                <Dividir />
              </div>
            </Route>
          </Switch>
        </Router>
      </div>
    )
  }
}

export default Calculadora;