import React, { Component } from 'react';

import './Operacoes.css'

class Dividir extends Component {

  constructor(props) {
    super(props);

    this.state = {
      operacao: "dividir",
      numero_um: "",
      numero_dois: "",
      numero_resultado: 0
    }

    this.dividir = this.dividir.bind(this)

    this.clear = this.clear.bind(this)
  }

  dividir() {
    let numero_resultado = parseInt(this.state.numero_um)/parseInt(this.state.numero_dois);

    this.setState({numero_resultado});
  }

  clear() {
    this.setState({
      numero_um: "",
      numero_dois: "",
      numero_resultado: 0     
    });
  }

  render() {
    return (
      <div>
        <h1 className="title_operacao">Operação: {this.state.operacao}</h1>
        <div className="content_input">
          <div className="content_numeros">
            <input className="numero_input" value={this.state.numero_um} type="text" onChange={(event)=> {this.setState({numero_um:event.target.value})}}></input>
            <input className="numero_input" value={this.state.numero_dois} type="text" onChange={(event)=> {this.setState({numero_dois:event.target.value})}}></input>
          </div>
          <input className="button_resultado" type="button" value={this.state.operacao} onClick={this.dividir}></input>
          <input className="resultado" type="text" value={this.state.numero_resultado}></input>
          <input className="button_clear" type="button" value="limpar" onClick={this.clear}></input>
        </div>
      </div>
    )
  }
}

export default Dividir;