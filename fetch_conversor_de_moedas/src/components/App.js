import React, { Component } from 'react';
import Conversor from './Conversor';

class App extends Component {
  render() {
    return (
      <div>
        <Conversor moeda_um="BRL" moeda_dois="USD"/>
      </div>
    )
  }
}

export default App;