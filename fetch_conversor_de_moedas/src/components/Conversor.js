import React, { Component } from 'react';
import './Conversor.css';

export default class Conversor extends Component {

  constructor(props) {

    super(props);

    this.state = {
      moeda_um_value: "",
      moeda_dois_value: 0
    }

    this.converter = this.converter.bind(this)

  }

  converter() {

    let de_para = `${this.props.moeda_dois}-${this.props.moeda_um}`;

    let url = `https://economia.awesomeapi.com.br/all/${de_para}`;

    fetch(url)
    .then(res=>{
      return res.json()
    })
    .then(json=>{
      let cotacao = json[this.props.moeda_dois].high;
      let moeda_dois_value = (parseFloat(this.state.moeda_um_value)/cotacao).toFixed(1);
      this.setState({moeda_dois_value})
    })

  }

  render() {
    return (
      <div className="body_conversor">
        <p className="conversor">Conversor de dinheiro</p>
        <p className="cotacao">{this.props.moeda_um} para {this.props.moeda_dois}</p>

        <input type="text" className="moeda_um" onChange={(event)=> {this.setState({moeda_um_value:event.target.value})}}></input>
        <input type="button" className="botao_converter" value="converter" onClick={this.converter}></input>
        <br/>
        <input type="text" className="moeda_dois" value={this.state.moeda_dois_value}></input>
      </div>
    )
  }
}