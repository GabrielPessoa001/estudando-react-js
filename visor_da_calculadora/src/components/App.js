import React from 'react';

import Calculadora from './Calculadora'

class App extends React.Component {

  render() {
    return (
      <div>
        <Calculadora />
      </div>
    )
  }

}

export default App;