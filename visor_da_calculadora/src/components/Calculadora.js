import React from 'react';

import './../stylesheets/Calculadora.css';

class Calculadora extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      number_visor_value: "",
      number_um: 0,
      number_dois: 0,
      operation: ""
    }

    this.mudar_visor = this.mudar_visor.bind(this)
    this.operation = this.mudar_visor.bind(this)
    this.clear = this.clear.bind(this)
  }

  mudar_visor(value) {
    let number_visor = 1;
    this.setState({number_visor_value: number_visor})
  }

  clear() {
    this.setState({
      number_visor_value: "",
      number_um: 0,
      number_dois: 0,
      operation: ""
    }) 
  }

  operation() {
    let number_um = 0
    let number_dois = 0
  }

  render() {
    return (
      <div className="calculadora">
        <div className="visor">
          <p className="clear_visor" onClick={this.clear}>C</p>
          <p className="number_visor">{this.state.number_visor_value}</p>
        </div>
        <div className="teclado">
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(1)})}>1</p>
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(4)})}>4</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(7)})}>7</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value+"+"})}>+</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value+"x"})}>x</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(3)})}>2</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(5)})}>5</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(8)})}>8</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(0)})}>0</p>
          <p className="item_calculadora" onClick={this.operation}>=</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(3)})}>3</p>
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(6)})}>6</p>
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value.concat(9)})}>9</p>          
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value+"-"})}>-</p>
          <p className="item_calculadora" onClick={(event)=> this.setState({number_visor_value: this.state.number_visor_value+"÷"})}>÷</p>                    
        </div>
      </div>
    )
  }
}

export default Calculadora;